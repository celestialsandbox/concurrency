/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

/**
 *
 * @author selvy
 */
public class CreationMetrics
{
    public  static  void    main( String[] args )
    {
        CreationMetrics cm = new CreationMetrics();
        
        // Massive difference in the metrics between these two methods
        // So there is definitely a gain in reusing objects
        cm.createAndUse();
        
        cm.reuseFromPool();
    }

    private void createAndUse()
    {
        long timeIn = System.currentTimeMillis();
        for( int x = 0; x < 1000; x++ )
        {
            Person pp = new Person("first:"+x, "last:"+x);        
        }
        long timeOut = System.currentTimeMillis();
        
        System.out.println("Time to execute: " + (timeOut-timeIn) + "ms");
    }

    private void reuseFromPool()
    {
        Person[] pp = new Person[1000];
        for( int x = 0; x < 1000; x++ )
        {
            pp[x] = new Person("first:"+x, "last:"+x);        
        }

        long timeIn = System.currentTimeMillis();
        for( int x = 0; x < 1000; x++ )
        {
            pp[x].setFirstName(x+":first").setLastName(x+":last");        
        }
        long timeOut = System.currentTimeMillis();
        
        System.out.println("Time to execute: " + (timeOut-timeIn) + "ms");
    }
}

class   Person
{
    private String  firstName;
    private String  lastName;
    
    public  Person( String fn, String ln )
    {
        firstName = fn;
        lastName = ln;
    }

    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public Person setFirstName(String firstName)
    {
        this.firstName = firstName;
        return this;
    }

    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public Person setLastName(String lastName)
    {
        this.lastName = lastName;
        return this;
    }   
}