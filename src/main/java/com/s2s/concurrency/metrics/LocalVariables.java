/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

/**
 *
 * @author selvy
 */
public class LocalVariables
{
    public  static  void    main( String[] args )
    {
        LocalVariables lv = new LocalVariables();
        
        lv.declareSomeLocalVariables();
    }

    private void declareSomeLocalVariables()
    {
        int xx = 44;
        int yy = 77;
        
        for( int idx = xx; idx < yy; idx++)
            System.out.println( xx + yy );
    }
    
}
