/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

/**
 *
 * @author Selvyn
 * 
 * Use this file to gather some basic metrics before measuring thread usage using the 
 * other files
 * 
 * When run with the profile (java -X switches), 6 threads run, 1,810 classes are
 * loaded, and 134Mb of heap is allocated
 * 
 */
public class BasicMetrics
{
    public  static  void    main( String[] args )
    {
        BasicMetrics bm = new BasicMetrics();
        
        bm.doForLoop();
    }
    
    public  void    doForLoop()
    {
        for(int x = 0; x < 100; x++ )
            System.out.println("Hello world");
    }
}
