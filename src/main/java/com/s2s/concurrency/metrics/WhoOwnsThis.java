/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author selvy
 */
public class WhoOwnsThis
{

    static public void main(String[] args)
    {
        WhoOwnsThis cb = new WhoOwnsThis();

        Callable<Boolean> runner = null;

        try
        {
            runner = cb.createInnerAnonymousCodeBlock();
            System.out.println("With Anonymous Clode Block:" + runner.call());
            
            runner = cb.createLambdaInnerCodeBlock();
            System.out.println("With Lambda Clode Block:" + runner.call());
            System.out.println("Lambda Clode Block vs Runner:" + (cb == runner));
        } catch (Exception ex)
        {
            Logger.getLogger(CodeBlocksWithLambdas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Callable<Boolean> createInnerAnonymousCodeBlock()
    {
        Object outerThis = this;
        
        Callable<Boolean> runner = new Callable<>()
        {
            @Override
            public Boolean call()
            {
                // This should return false
                return outerThis == this;
            }
        };

        // Returing the runner to ensure it does not go out of scope
        return runner;
    }
    
    public  Callable<Boolean>    createLambdaInnerCodeBlock()
    {
        Object outerThis = this;
        
        Callable<Boolean> runner = () -> {
            // This should return true
            return outerThis == this;
        };
        System.out.println("Is the code block this:" + (runner==this));
        /* This returns false??
         * So as a design decision, the Java treats the this in a lambda as the this
         * of the enclosing class even though the labd does result in the creation 
         * of an object.  The this of lambda is only visible if you preserve the 
         * lambda instance in a variable or return it from the enclosing method
         */
                
        // Returing the runner to ensure it does not go out of scope
        return runner;
    }


}
