/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

import java.util.function.Function;

/**
 *
 * @author selvy
 */
public class AnonymousClassExample
{
    Function<String, String> format = new Function<>()
    {
        @Override
        public String apply(String input)
        {
            return Character.toUpperCase(input.charAt(0)) + input.substring(1);
        }
    };
}
