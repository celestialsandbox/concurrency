/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency.metrics;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author selvyn
 *
 * Run this code under a profiler
 * 
 * Alter the count to see if there is any increase in memory consumption, the answer 
 * is yes, but the size of the anonymous instance 
 * (has a name <ounter class>.<anonymous instance id> which is about 24 bytes for
 * one iteration, plus the anonymous instance has to be loaded via the class loader
 *
 */
public class CodeBlocksWithAnonymousClass 
{
    private static  final int COUNT = 1;

    static  public  void    main( String[] args )
    {
        CodeBlocksWithAnonymousClass cb = new CodeBlocksWithAnonymousClass();
        
        for( int x = 0; x < COUNT; x++)
        {
            Callable<Integer> runner = cb.createInnerCodeBlock(x);
            
            try
            {
                System.out.println( runner.call());
            } catch (Exception ex)
            {
                Logger.getLogger(CodeBlocksWithLambdas.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
    }
    
    public  Callable<Integer>    createInnerCodeBlock(int i)
    {
        Callable<Integer> runner = new Callable<Integer>()
        {
            @Override
            public  Integer call()
            {
                int x = i;
            
                return i;
            }
        };
        
        // Returing the runner to ensure it does not go out of scope
        return runner;
    }
    
}
