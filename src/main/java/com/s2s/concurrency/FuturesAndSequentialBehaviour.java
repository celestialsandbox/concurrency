/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Selvyn
 */
public class FuturesAndSequentialBehaviour
{
    public static void main(String[] args) throws Exception
    {
        // Create a queue with enough space to accomodate all our tasks
        // The constructor's capacity value canno be smaller than the max pool size 
        // of the ThreadPoolExecutor
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(20);
        
        // Create a thread pool, initial pool = 6, max pool = 10, keep-alive = 1sec
        ThreadPoolExecutor testExecutor = new ThreadPoolExecutor(6, 10, 1,
                TimeUnit.SECONDS, blockingQueue);
        
        // Create an array that will hold the Future objects returned from the .submit()
        // of the ThreadPoolExecutor()
        List<Future<String>> futures = new ArrayList<>();
        
        // Create the tasks, assigning each one a unique number and attaching a Callable
        // object to that task
        // The .submit() is non-blocking
        for (int i = 0; i < 20; i++)
        {
            Future<String> testFuture = testExecutor.submit(new MyCallable(i));
            futures.add(testFuture);
        }
        
        // Wait for each task to complete - the .get() is blocking and waits for a task
        // to complete.  One a task completes, it's not destroyed/target for reuse 
        // until the .get() is called
        for (Future<String> testFuture : futures)
        {
            System.out.println("Output Returned is : " + testFuture.get());
        }
    }
}

class MyCallable implements Callable<String>
{
    int id;

    public MyCallable(int id)
    {
        this.id = id;
    }

    @Override
    public String call() throws Exception
    {
        System.out.println("Running " + this.id);
        return "Called " + this.id;
    }
}
