package com.s2s.concurrency;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
import java.lang.InterruptedException;

public class ThreadClashSynchronised
{

    private String someData = "Hello World";
    private boolean continueToRun = true;

    public static void main(String[] args)
    {
        ThreadClashSynchronised threadClashSynchronised1 = new ThreadClashSynchronised();

        threadClashSynchronised1.execute();
    }

    private void execute()
    {
        Thread t1 = new Thread()
        {
            public void run()
            {
                while (ThreadClashSynchronised.this.continueToRun)
                {
                    /*
                        This version isn't smart because the scope of the lock
                        is on the entire object "ThreadClashSynchronised.this"
                    
                        It would be mean that all threads accessing any part
                        of ThreadClashSynchronised.this where snchronization has
                        been used would be forced to contend with a monitor
                    */
                    synchronized (ThreadClashSynchronised.this)
                    {
                        ThreadClashSynchronised.this.someData = "Handler One";

                        ThreadClashSynchronised.this.someData
                                = ThreadClashSynchronised.this.someData + ", Handler One";
                    }
                }
            }
        };

        Thread t2 = new Thread()
        {
            public void run()
            {
                while (ThreadClashSynchronised.this.continueToRun)
                {
                    synchronized (ThreadClashSynchronised.this)
                    {
                        ThreadClashSynchronised.this.someData = "Another Handler";

                        ThreadClashSynchronised.this.someData
                                = ThreadClashSynchronised.this.someData + ", Another Handler";
                    }
                }
            }
        };

        Thread mainLoop = new Thread()
        {
            public void run()
            {
                for (int i = 0; i < 999; i++)
                {
                    System.out.println(ThreadClashSynchronised.this.getData());
                }
                ThreadClashSynchronised.this.continueToRun = false;
            }
        };

        mainLoop.start();
        t1.start();
        t2.start();
    }

    private synchronized String getData()
    {
        return someData;
    }
}
