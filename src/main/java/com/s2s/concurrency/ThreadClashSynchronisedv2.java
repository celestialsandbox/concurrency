package com.s2s.concurrency;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
import java.lang.InterruptedException;

public class ThreadClashSynchronisedv2
{

    private String someData = "Hello World";
    private boolean continueToRun = true;

    public static void main(String[] args)
    {
        ThreadClashSynchronisedv2 threadClashSynchronised = new ThreadClashSynchronisedv2();

        threadClashSynchronised.execute();
    }

    private void execute()
    {
        Thread t1 = new Thread(() ->
        {
            while (ThreadClashSynchronisedv2.this.continueToRun)
            {
                synchronized (ThreadClashSynchronisedv2.this.someData)
                {
                    ThreadClashSynchronisedv2.this.someData = "Handler One";
                    
                    ThreadClashSynchronisedv2.this.someData
                            = ThreadClashSynchronisedv2.this.someData + ", Handler One";
                }
                
            }
        });

        Thread t2 = new Thread(() ->
        {
            while (ThreadClashSynchronisedv2.this.continueToRun)
            {
                /*
                    This version narrows the scope of the lock to the object in
                    question "ThreadClashSynchronisedv2.this.someData"
                
                    So the only time the monitor comes into play is when someData
                    is accessed.
                */
                synchronized (ThreadClashSynchronisedv2.this.someData)
                {
                    ThreadClashSynchronisedv2.this.someData = "Another Handler";

                    ThreadClashSynchronisedv2.this.someData
                            = ThreadClashSynchronisedv2.this.someData + ", Another Handler";
                }
            }
        });

        Thread mainLoop = new Thread()
        {
            public void run()
            {
                for (int i = 0; i < 999; i++)
                {
                    System.out.println(ThreadClashSynchronisedv2.this.getData());
                }
                ThreadClashSynchronisedv2.this.continueToRun = false;
            }
        };

        mainLoop.start();
        t1.start();
        t2.start();
    }

    /*
     *  This will not have the desired affect because you are locking the wrong object
     */
    private synchronized String getData()
    {
        return someData;
    }
}
