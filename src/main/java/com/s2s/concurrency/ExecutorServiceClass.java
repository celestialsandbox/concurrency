package com.s2s.concurrency;

import static java.lang.Thread.sleep;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceClass
{
    public  void    execute()
    {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        executor.submit(()->{
            for(int x=0; x < 1000; x++){
            	Thread.yield();
                System.out.println("thread 1: " + x );}
                });
        executor.submit(()->{
            for(int x=0; x < 1000; x++ ){
            	Thread.yield();
                System.out.println("thread 2: " + x);}
                });
        executor.submit(()->{
            for(int x=0; x < 1000; x++ ){
            	Thread.yield();
                System.out.println("thread 3: " + x);}
                });

        /*
         *  If you invoke executor.submit(...) more times than what you specified
         *  in newFixedThreadPool( <val> ), there is no guarantee that the additional 
         *  threads will execute until the previous have run, you could force thread 3
         *  changing its priority
         */

        executor.shutdown();
    }

    public static void main(String[] args)
    {
    	ExecutorServiceClass esc = new ExecutorServiceClass();

        esc.execute();
    }

}
