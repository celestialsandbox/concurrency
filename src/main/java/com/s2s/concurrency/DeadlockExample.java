/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author Selvyn
 */
public class DeadlockExample
{
    public static void main(String[] args)
    {
        DeadlockExample dle = new DeadlockExample();

        dle.execute();
    }

    private void execute()
    {
        DeadlockDetector deadlockDetector = 
                new DeadlockDetector(new DeadlockHandlerToConsole(), 3, TimeUnit.SECONDS);
        deadlockDetector.start();
        
        final Object lock1 = new Object();
        final Object lock2 = new Object();        
    
        Thread thread1 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                synchronized (lock1)
                {
                    System.out.println("Thread1 acquired lock1");
                    try
                    {
                        TimeUnit.MILLISECONDS.sleep(500);
                    } catch (InterruptedException ignore){}
                    synchronized (lock2)
                    {
                        System.out.println("Thread1 acquired lock2");
                    }
                }
            }

        });

        Thread thread2 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                synchronized (lock2)
                {
                    System.out.println("Thread2 acquired lock2");
                    synchronized (lock1)
                    {
                        System.out.println("Thread2 acquired lock1");

                    }
                }
            }
        });
        thread1.start();
        thread2.start();
    }

}
