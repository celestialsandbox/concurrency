/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 *
 * @author Selvyn
 */
public class FuturesAndPromises_v1
{
    static  ExecutorService ex = Executors.newFixedThreadPool(10);
    static  BlockingQueue<Result> tvResults = new ArrayBlockingQueue<>(10);

    static  class   Result
    {
        String entry;
        String exit = "-1";
        
        public  Result( String x )
        {
            entry = x;
        }
    }

    public static void main(String[] args)
    {
        for (int x = 0; x < 10; x++)
        {
            String id = Integer.toString(x);
            Result rr = new Result(id);
            doActions( rr );
        }
        
        new Thread(()->{
                while(true)
                {
                    Result rr;
                    try
                    {
                        rr = tvResults.take();
                        System.out.println(rr.exit);
                    } catch (InterruptedException ex){}
                }
        }).start();
    }

    public static Result doActions( Result rr )
    {
        Supplier<Result> momsPurse = () ->
        {
            try
            {
                System.out.println("starting the calculation on " + rr.entry);
                Thread.sleep(3000); //simulate some kind of delay
                rr.exit = "Object: " + rr.entry;
                System.out.println("calculations complete on " + rr.entry);
            } catch (InterruptedException e){}

            return rr;
        };

        CompletableFuture<Result> promise
                = CompletableFuture.supplyAsync(momsPurse, ex);

        System.out.println("futures setup");

        promise.thenAcceptAsync(res -> {
            try
            {
                tvResults.put(res);
            } catch (InterruptedException ex){}
        });

        return null;
    }
}
