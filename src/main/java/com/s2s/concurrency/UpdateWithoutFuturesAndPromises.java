/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 *
 * @author Selvyn
 */
public class UpdateWithoutFuturesAndPromises
{
    static  ExecutorService ex = Executors.newFixedThreadPool(10);
    static  BlockingQueue<Result> tvResults = new ArrayBlockingQueue<>(10);
    static  final   int NUM_RESULTS = 100;

    static  class   Result
    {
        String entry;
        String exit = "-1";
        
        public  Result( String x )
        {
            entry = x;
        }
    }

    public static void main(String[] args)
    {
        new Thread(()->
        {
            for (int x = 0; x < NUM_RESULTS; x++)
            {
                String id = Integer.toString(x);
                Result rr = new Result(id);
                doActions( rr );
            }
            System.out.println("Leaving thread A");
        }).start();
        
        new Thread(()->{
                for (int x = 0; x < NUM_RESULTS; x++)
                {
                    Result rr;
                    try
                    {
                        rr = tvResults.take();
                        System.out.println(rr.exit);
                    } catch (InterruptedException ex){}
                }
            System.out.println("Leaving thread B");
        }).start();
        
        //System.exit(0);
    }

    public static void doActions( Result rr )
    {
        ex.submit(() ->
        {
            try
            {
                System.out.println("starting the calculation on " + rr.entry);
                Thread.sleep(3); //simulate some kind of delay
                rr.exit = "Object: " + rr.entry;
                tvResults.put(rr);
                System.out.println("calculations complete on " + rr.entry);
            } catch (InterruptedException e){}
        });
    }
}
