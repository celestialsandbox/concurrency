package com.s2s.concurrency;

import java.io.FileOutputStream;
import java.io.*;

/*
 * Created on 25-Aug-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Selvyn
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RunnableClass	implements Runnable
{
    private final int id;
    public	static	int count = 0;
    static private FileOutputStream fos = null;
    static private DataOutputStream dos = null;
    static private	File ff;
    
    public RunnableClass( int id )
    {
        this.id = id;
    }

    static	public	void	setup()
    {
        ff = new File("c:\\tmp\\test.txt");
        try
        {
            ff.createNewFile();
            fos = new FileOutputStream(ff);
            dos = new DataOutputStream( fos ) ;
        }
        catch (IOException e)
        {
        }
    }
    
    public void run()
    {
        for(int idx=0; idx<400; idx++)
        {
            try
            {
	        String output = "thread[" + id + "]: " + idx;
	        System.out.println( output );
                dos.write( (output+"\n").getBytes() );
                count++;
                output = "count[" + id + "]: " + count;
                System.out.println( output );
                dos.write( (output+"\n").getBytes() );
            } 
            catch (IOException e)
            {
            }
        }
    }
    
    public static void main(String[] args)
    {
        RunnableClass.setup();
        RunnableClass rc1 = new RunnableClass( 1 );
        Thread t1 = new Thread( rc1 );
        
        RunnableClass rc2 = new RunnableClass( 2 );
        Thread t2 = new Thread( rc2 );

        RunnableClass rc3 = new RunnableClass( 3 );
        Thread t3 = new Thread( rc3 );
        
        t1.start();
        t2.start();
        t3.start();
        
        for(int i=0; i<1000; i++)
        {
            System.out.println( "main: " + i );
            count++;
            System.out.println( "count: " + count );
        }
    }
}
