/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Selvyn
 */
public class DeadlockDetector
{
    private final ThreadDeadlockHandler itsDeadlockHandler;
    private final long itsPeriod;
    private final TimeUnit itsUnit;
    private final ThreadMXBean itsMBean = ManagementFactory.getThreadMXBean();
    private final ScheduledExecutorService itsScheduler = Executors.newScheduledThreadPool(1);

    final Runnable itsDeadlockChecker = new Runnable()
    {
        @Override
        public void run()
        {
            long[] deadlockedThreadIds = DeadlockDetector.this.itsMBean.findDeadlockedThreads();

            if (deadlockedThreadIds != null)
            {
                ThreadInfo[] threadInfos
                        = DeadlockDetector.this.itsMBean.getThreadInfo(deadlockedThreadIds);

                DeadlockDetector.this.itsDeadlockHandler.handleDeadlock(threadInfos);
            }
        }
    };

    public DeadlockDetector(final ThreadDeadlockHandler deadlockHandler,
            final long period, final TimeUnit unit)
    {
        this.itsDeadlockHandler = deadlockHandler;
        this.itsPeriod = period;
        this.itsUnit = unit;
    }

    public void start()
    {
        itsScheduler.scheduleAtFixedRate(
                itsDeadlockChecker, itsPeriod, itsPeriod, itsUnit);
    }
}
