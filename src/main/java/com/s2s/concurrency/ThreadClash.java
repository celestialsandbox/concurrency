package com.s2s.concurrency;


import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
public class ThreadClash
{

    private String someData = "Hello World";
    private boolean continueToRun = true;

    public static void main(String[] args)
    {
        ThreadClash threadClash1 = new ThreadClash();
        threadClash1.execute();
    }

    private void execute()
    {
        Thread t1 = new Thread()
        {
            public void run()
            {
                while (ThreadClash.this.continueToRun)
                {
                    ThreadClash.this.someData = "Handler One";
                    ThreadClash.this.someData = ThreadClash.this.someData + ", Handler One";
                }
            }
        };

        Thread t2 = new Thread()
        {
            public void run()
            {
                while (ThreadClash.this.continueToRun)
                {
                    ThreadClash.this.someData = "Another Handler";
                    ThreadClash.this.someData = ThreadClash.this.someData + ", Another Handler";
                }
            }
        };

        Thread mainLoop = new Thread()
        {
            public void run()
            {
                for (int i = 0; i < 999; i++)
                {
                    System.out.println("[" + ThreadClash.this.someData + "]");
                }
                ThreadClash.this.continueToRun = false;

                System.exit(0);
            }
        };

        mainLoop.start();
        t1.start();
        t2.start();
    }
}
