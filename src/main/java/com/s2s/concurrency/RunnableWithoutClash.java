package com.s2s.concurrency;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
public class RunnableWithoutClash	implements	Runnable
{
    private int id;
    public	static	int count = 0;
    
    public RunnableWithoutClash( int id )
    {
        this.id = id;
    }
    
    public void run()
    {
        for(int idx=0; idx<400; idx++)
        {
            FileStuff fs = FileStuff.getInstance();
            
            fs.dumpData(count, idx, id);
        }
    }
    
    public static void main(String[] args)
    {
        FileStuff fs = FileStuff.getInstance();
        fs.setup();

        RunnableWithoutClash rc1 = new RunnableWithoutClash( 1 );
        Thread t1 = new Thread( rc1 );
        
        RunnableWithoutClash rc2 = new RunnableWithoutClash( 2 );
        Thread t2 = new Thread( rc2 );

        RunnableWithoutClash rc3 = new RunnableWithoutClash( 3 );
        Thread t3 = new Thread( rc3 );
        
        t1.start();
        t2.start();
        t3.start();
        
        for(int i=0; i<1000; i++)
        {
            System.out.println( "main: " + i );
            count++;
            System.out.println( "count: " + count );
        }
    }
}

//	This is a singleton class
class	FileStuff
{
    private FileOutputStream fos = null;
    private DataOutputStream dos = null;
    private	File ff;
    private final Object syncObject = new Object();
    
    static	private	FileStuff self = null;

    private	FileStuff()
    {
    }
    
    static	public	FileStuff	getInstance()
    {
        if( self == null )
            self = new FileStuff();
        
        return self;
    }
    
    public	void	setup()
    {
        ff = new File("c:\\tmp\\test.txt");
        try
        {
            ff.createNewFile();
            fos = new FileOutputStream(ff);
            dos = new DataOutputStream( fos ) ;
        }
        catch (IOException e)
        {
        }
    }

    /*
     Run the code as it is, then study the output in the textfile c:\tmp\test.txt
     On the first run, you should notice that the output looks like 
        thread[3] for idx: 8
        thread[2] for idx: 15
     
     The implication being that the dumpData can be completely run it's interrupted
     by another thread.  A thread can be swapped on any IO or non-atomic operation.
    
     Interestingly i++ is not atomic and therefore before it completes it might be 
     interrupted.
    
     Now swap the methods and exam the results in the test.txt file
    */
    //synchronized		public void dumpData(int count, int idx, int id )
    public void dumpData(int count, int idx, int id )
    {
        try
        {
            //synchronized( this )
            //synchronized( syncObject )
            {
                String output = "thread[" + id + "] for idx: " + idx;
                System.out.println( output );
                dos.write( (output+"\n").getBytes() );
                count++;
                output = "count[" + id + "]: " + count;
                System.out.println( output );
                dos.write( (output+"\n").getBytes() );
            }
        } 
        catch (IOException e)
        {
        }
    }

    public DataOutputStream getDataOutputStream()
    {
        return dos;
    }
    public File getFile()
    {
        return ff;
    }
    public FileOutputStream getFileOutputStream()
    {
        return fos;
    }
}
