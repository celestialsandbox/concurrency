package com.s2s.concurrency;


/**
 * Title:        Java Courseware
 * Description:
 * Copyright:    Copyright (c) 2010
 * Company:      TROy Software Development
 *
 * @author Selvyn Wright
 * @version 1.0
 */
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WorkerDispatcherUsingLocks
{
    private final SharedDataStore dataStore = new SharedDataStore();
    
    private ExecutorService executor;
    private Callable<SharedDataStore> callableProducer;
    private LinkedBlockingQueue<Future<SharedDataStore>> itsProducers = new LinkedBlockingQueue<>();
    private Future<SharedDataStore> producerFuture_1;
    private Future<SharedDataStore> producerFuture_2;
    private Callable<Boolean> callableConsumer;
    private Future<Boolean> consumerFuture;

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {
        private String  itsThreadID;
        private ReentrantLock itsLock = new ReentrantLock();
        // I have purposely used a non-synchronized collection to illustrates the 
        // hazards of not understanding how concurrency can affect your code
        private final ArrayList<String> db = new ArrayList<>();
        
        public void clearDataStore()
        {
            // Hazardous operation, called from a thread, it modifies the collection
            // structure
            db.clear();
        }

        /*
         * This method works in an intermittement manner, examine the console output
         * and you should see that it doesn't quite behave as you think.  The values
         * stored in the db object which is type ArrayList<String> is supposed to
         * be filled with a collection of 100 objects from one thread, then another
         * collection of 100 objects from another, and so on so on. What actully
         * happens is that at random points the objets from the different threads 
         * get thrown into the collection, not in the order we want.
         *
         * The application also intermittently hangs.  Why??
        */
        public void updateDB( long threadID )
        {
            System.out.println("Thread[" + threadID + "] attempting to lock data store");
            // for a single thread producer, the tryLock/lock calls are
            // redundant because of the use of future objects, but if you 
            // examine the code we have multiple producers
            System.out.println("Thread[" + threadID + "] updating the data store");
            for (int i = 0; i < 100; i++)
            {
                // Hazardous operation, called from a thread, it modifies the collection
                // structure
                db.add("Thread[" + threadID + "]=" + i);
            }
            System.out.println("Thread[" + threadID + "] update completed");
        }

        public  ArrayList<String> getData()
        {
            return db;
        }
    }

    private class DispatcherThread
    {
        public  DispatcherThread()
        {
        }
        
        /*
         * There is a strong possiility that this method will hang with a daemon
         * or rogue thread
        */
        public void execute()
        {
            try
            {
                for (int idx = 0; idx < 4; idx++)
                {
                    Future<SharedDataStore> producerFuture = itsProducers.take();
                    SharedDataStore dataStore = producerFuture.get();
                
                    printData( dataStore );
                    
                    // see note above on design of getData()
                    dataStore.clearDataStore();
                    
                    // Each time this method is called the lambda "callableProducer"
                    // is given as the method to the waiting thread in the executor
                    itsProducers.put( executor.submit(callableProducer) );
                }
            } 
            catch (InterruptedException | ExecutionException ex)
            {
                Logger.getLogger(WorkerDispatcherUsingLocks.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void printData(SharedDataStore theDataStore)
        {
            ArrayList<String> localDB = theDataStore.getData();

            int length = localDB.size();

            localDB.forEach((str) ->
            {
                System.out.println( str );
            });
        }
    }
    
    private long    tstart, tend;
    public  void    execute()
    {
        try
        {
            // We'll setup 3 executors, 2 x producers, 1 x consumer
            executor = Executors.newFixedThreadPool(3);
            callableProducer = () -> {
                tstart = System.currentTimeMillis();
                dataStore.updateDB( Thread.currentThread().getId());
                return dataStore;
            };
            // Setup producer 1
            itsProducers.put( executor.submit(callableProducer));
            // Setup producer 2
            itsProducers.put( executor.submit(callableProducer));
            
            callableConsumer = () -> {
                DispatcherThread dt = new DispatcherThread();
                dt.execute();
                executor.shutdown();
                tend = System.currentTimeMillis();
                System.out.println( "Time to run: " + (tend-tstart));
                return false;
            };
            consumerFuture = executor.submit(callableConsumer);
        } catch (InterruptedException ex)
        {
            Logger.getLogger(WorkerDispatcherUsingLocks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args)
    {
        WorkerDispatcherUsingLocks waitAndNotify = new WorkerDispatcherUsingLocks();

        waitAndNotify.execute();
    }
}
