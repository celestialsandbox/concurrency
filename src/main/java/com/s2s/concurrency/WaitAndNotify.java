package com.s2s.concurrency;



/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 19-Sep-2005
 */

/*
 *  This example attempts to capture a basic consumer/producer problem usig what
 *  is termed the work thread pattern.
 *  
 *  It has numerous weakeness
 *  Badly designed coordinattion between the work and dispatcher threads
 *  An infinite loop dispatchResults() that's wasting process power
 *  Synchronize on the methods which means the whole "this" is guarded
 *  The use of a collection where all the methods are synchroized
 *  Deadloack depending on which thread starts first
 *
 *  With each new version _P2, _P3, _P4, _P5 and finally WorkerDispatcherUsingLocks
 *  we work through a change and discuss it's merits and weaknesses
 */
import   java.util.Vector;


public class WaitAndNotify
{
   private  boolean  jobCompleted = false;
   private final  Vector   db = new Vector();
   private final  DispatcherThread  dispatcher = new DispatcherThread();
   private final  WorkerThread worker = new WorkerThread();

   private  class WorkerThread   extends  Thread
   {
      public   void  run()
      {
         WaitAndNotify.this.updateDB();
      }
   }

   private  class DispatcherThread   extends  Thread
   {
      public   void  run()
      {
         WaitAndNotify.this.dispatchResults();
      }
   }

   private  synchronized   void  updateDB()
   {
      for( int i = 0; i < 10; i++ )
      {
         db.addElement( "Handler One" + i );
      }
      jobCompleted = true;
   }

   private  synchronized   void  dispatchResults()
   {
      while( ! jobCompleted )
      {
        ;
      }
      jobCompleted = false;
      int length = db.size();

      for( int i = 0; i < length; i++ )
      {
         String val = (String)db.elementAt(  i );
         System.out.println( val );
      }
   }

   private  void  execute()
   {
       // if the program doesn't run, try swapping these lines???
      dispatcher.start();
      worker.start();
   }

   public static void main(String[] args)
   {
      WaitAndNotify waitAndNotify1 = new WaitAndNotify();

      waitAndNotify1.execute();
   }
}