package com.s2s.concurrency;


import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
public class ThreadClashv3
{

    private String someData = "Hello World";
    private boolean continueToRun = true;

    public static void main(String[] args)
    {
        ThreadClashv3 threadClash = new ThreadClashv3();
        threadClash.execute();
    }

    private void execute()
    {
        Thread t1 = new Thread()
        {
            public void run()
            {
                while (ThreadClashv3.this.continueToRun)
                {
                    ThreadClashv3.this.someData = "Handler One";
                    ThreadClashv3.this.someData = ThreadClashv3.this.someData + ", Handler One";
                }
            }
        };

        Thread t2 = new Thread()
        {
            public void run()
            {
                while (ThreadClashv3.this.continueToRun)
                {
                    ThreadClashv3.this.someData = "Another Handler";
                    ThreadClashv3.this.someData = ThreadClashv3.this.someData + ", Another Handler";
                }
            }
        };

        Thread mainLoop = new Thread()
        {
            public void run()
            {
                for (int i = 0; i < 999; i++)
                {
                    System.out.println("[" + ThreadClashv3.this.someData + "]");
                    try
                    {
                        sleep(1);
                    } catch (InterruptedException ex)
                    {
                        Logger.getLogger(ThreadClash.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ThreadClashv3.this.continueToRun = false;

                System.exit(0);
            }
        };

        mainLoop.start();
        t1.start();
        t2.start();
    }
}
