package com.s2s.concurrency;



/**
 * Title:        Java Courseware
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      TROy Software Development
 * @author Selvyn Wright
 * @version 1.0
 */

import java.util.ArrayList;

public class ThreadsSingletonsv2
{
   private  static int ThreadCount = 0;
   private static final ThreadLocal singletons = new ThreadLocal<ArrayList<String>>()
   {
        @Override protected ArrayList<String> initialValue() 
        {
                 return new ArrayList<>();
        }
   };

   public static ArrayList getWarehouse()
   {
      //first time this is called will return null
      ArrayList<String> v = (ArrayList<String>)singletons.get();
      if (v == null)
      {
         v = new ArrayList<>();
         // set the vector as the singleton for the current thread
         singletons.set(v);
      }
      return v;
   }

   static   private  class InnerThread  implements  Runnable
   {
      private  String id;
      public   InnerThread( String id ){ this.id = id; }

      public void run()
      {
       ArrayList<String> v = ThreadsSingletons.getWarehouse();
       v.add( id );
       v = ThreadsSingletons.getWarehouse();
       v.add( id );
       // give another thread a chance to run...
       try{Thread.sleep(2000);}catch(Exception e){}
       v = ThreadsSingletons.getWarehouse();
       v.add( id );
       System.out.println(v);
      }
   }

   public static void main(String[] args)
   {
      try
      {
         for (int i = 0; i < 5; i++)
         {
            Thread t = new Thread(new InnerThread( "Thread: " + i ));
            // alter priorities to give some randomness to expected ordering
            t.setPriority( i + 3 );
            t.start();
         }
      }
      catch(Exception e){e.printStackTrace();}
   }
}
