package com.s2s.concurrency;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 19-Sep-2005
 */

/*
 * In this version we have redesigned the code so that there is a class called
 * SharedDataStore whose sole responsibility is to handle threading issues, in
 * other words move it away from the code that simeply wants the data, so as to
 * reduce the coupling and remove inappropriate coupling in the code.
 * 
 * Notice the use of synchrnization around the methods of SharedDataStore.  Also
 * notice the use of wait() and notify() to coordinate the relationship between 
 * the worker (producer) thread and the dispatcher (consumer) thread.
 *
 * When you run this code, you should notice that irrespective of which thread
 * starts first you get the result you want.
 */
import java.util.Vector;

public class WaitAndNotify_P2
{

    private final DispatcherThread dispatcher = new DispatcherThread();
    private final WorkerThread worker = new WorkerThread();
    private final SharedDataStore dataStore = new SharedDataStore();

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {

        private Vector db = new Vector();
        private boolean jobCompleted = false;

        public synchronized void clearDataStore()
        {
            db.removeAllElements();

            notify();
        }

        public synchronized void updateDB()
        {
            System.out.println("updating the data store");

            for (int i = 0; i < 100; i++)
            {
                db.addElement("Handler One" + i);
            }
            jobCompleted = true;

            System.out.println("update completed");
            notify();
            try
            {
                wait();
            } 
            catch (InterruptedException e){}
        }

        // When this operation exits, the waiting thread now has access
        // to the datastore and the operation updateDB can now be called
        //
        // The call db.clear() above has to be done after the callee of getDB() has finished with
        // the data from this call
        public synchronized Vector getData()
        {
            while (!jobCompleted)
            {
                try
                {
                    wait();
                } 
                catch (InterruptedException e)
                {
                }
            }
            jobCompleted = false;

            return db;
        }
    }

    private class WorkerThread extends Thread
    {
        private Vector db = new Vector();

        @Override
        public void run()
        {
            for (;;)
            {
                updateDataStore();
            }
        }

        private void updateDataStore()
        {
            dataStore.updateDB();
        }

    }

    private class DispatcherThread extends Thread
    {
        @Override
        public void run()
        {
            for (int idx = 0; idx < 2; idx++)
            {
                getResults();

                // see note above on design of getData()
                dataStore.clearDataStore();
            }
            System.exit(0);
        }

        private void getResults()
        {
            Vector localDB = dataStore.getData();

            int length = localDB.size();

            for( Object str: localDB )
            {
                System.out.println( str );
            }
        }
    }

    private void execute1()
    {
        System.out.println("execute 1");

        worker.start();
        dispatcher.start();
    }

    private void execute2()
    {
        System.out.println("execute 2");

        dispatcher.start();
        worker.start();
    }

    public static void main(String[] args)
    {
        int version = 1;
        WaitAndNotify_P2 waitAndNotify = new WaitAndNotify_P2();

        if(version == 2)
        {
            waitAndNotify.execute1();
        } 
        else
        {
            waitAndNotify.execute2();
        }
    }
}
