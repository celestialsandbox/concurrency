package com.s2s.concurrency;

/*
 /**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */
public class ThreadClass	extends Thread
{
    private final	int id;
    public	ThreadClass( int id )
    {
        this.id = id;
    }
    public void run()
    {
        for(int i=0; i<1000; i++)
        {
            System.out.println( "thread[" + id + "]: " + i );
        }
    }
    
    public static void main(String[] args)
    {
        ThreadClass tc1 = new ThreadClass( 1 );
        ThreadClass tc2 = new ThreadClass( 2 );
        ThreadClass tc3 = new ThreadClass( 3 );
        
        tc1.start();
        tc2.start();
        tc3.start();
        
        for(int i=0; i<1000; i++)
        {
            //System.out.println( "main: " + i );
        }
    }
}
