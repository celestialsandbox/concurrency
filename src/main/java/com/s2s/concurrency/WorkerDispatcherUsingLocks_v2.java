package com.s2s.concurrency;


/**
 * Title:        Java Courseware
 * Description:
 * Copyright:    Copyright (c) 2010
 * Company:      TROy Software Development
 *
 * @author Selvyn Wright
 * @version 1.0
 */
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * This version throws a very interesting exception, read the stack trace to
 * determine what the problem is.
*/

public class WorkerDispatcherUsingLocks_v2
{
    private final SharedDataStore dataStore = new SharedDataStore();
    
    private ExecutorService executor;
    private Callable<SharedDataStore> callableProducer;
    private LinkedBlockingQueue<Future<SharedDataStore>> itsProducers = new LinkedBlockingQueue<>();
    private Future<SharedDataStore> producerFuture_1;
    private Future<SharedDataStore> producerFuture_2;
    private Callable<Boolean> callableConsumer;
    private Future<Boolean> consumerFuture;

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {
        private String  itsThreadID;
        private ReentrantLock itsLock = new ReentrantLock();
        private final ArrayList<String> db = new ArrayList<>();
        
        public void clearDataStore()
        {
            // Hazardous operation, called from a thread, it modifies the collection
            // structure
            db.clear();
        }

        public void updateDB( long threadID )
        {
           try
            {
                System.out.println("Thread[" + threadID + "] attempting to lock data store");
                // for a single thread producer, the tryLock/lock calls are
                // redundant because of the use of future objects
                if(itsLock.tryLock());
                {
                    System.out.println("Thread[" + threadID + "] updating the data store");
                    for (int i = 0; i < 100; i++)
                    {
                        db.add("Thread[" + threadID + "]=" + i);
                    }
                    System.out.println("Thread[" + threadID + "] update completed");
                }
            }finally
            {
                itsLock.unlock();
            }
        }

        public  ArrayList<String> getData()
        {
            return db;
        }
    }

    private class DispatcherThread
    {
        public  DispatcherThread()
        {
        }
        
        public void execute()
        {
            try
            {
                for (int idx = 0; idx < 4; idx++)
                {
                    //SharedDataStore dataStore = producerFuture.get();
                    Future<SharedDataStore> producerFuture = itsProducers.take();
                    /*
                     * When the future.get() is called, the thread goes into a 
                     * wait state, in this state it can check if an ExecutionException
                     * has been thrown
                     */
                    SharedDataStore dataStore = producerFuture.get();
                
                    printData( dataStore );
                    
                    // see note above on design of getData()
                    dataStore.clearDataStore();
                    
                    itsProducers.put( executor.submit(callableProducer) );
                }
            } 
            catch (InterruptedException | ExecutionException ex)
            {
                Logger.getLogger(WorkerDispatcherUsingLocks_v2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*
         * This method is no longer safe without a lock being used because their
         * could be multiple threads modifying the ArrayList<> whilst we are 
         * iterating over it's content
        */
        private void printData(SharedDataStore theDataStore)
        {
            ArrayList<String> localDB = theDataStore.getData();

            int length = localDB.size();

            localDB.forEach((str) ->
            {
                System.out.println( str );
            });
        }
    }
    
    private long    tstart, tend;
    public  void    execute()
    {
        try
        {
            // We'll setup 3 executors, 2 x producers, 1 x consumer
            executor = Executors.newFixedThreadPool(3);
            callableProducer = () -> {
                tstart = System.currentTimeMillis();
                dataStore.updateDB( Thread.currentThread().getId());
                return dataStore;
            };
            // Setup producer 1
            itsProducers.put( executor.submit(callableProducer));
            // Setup producer 2
            itsProducers.put( executor.submit(callableProducer));
            
            callableConsumer = () -> {
                DispatcherThread dt = new DispatcherThread();
                dt.execute();
                executor.shutdown();
                tend = System.currentTimeMillis();
                System.out.println( "Time to run: " + (tend-tstart));
                return false;
            };
            consumerFuture = executor.submit(callableConsumer);
        } catch (InterruptedException ex)
        {
            Logger.getLogger(WorkerDispatcherUsingLocks_v2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args)
    {
        WorkerDispatcherUsingLocks_v2 waitAndNotify = new WorkerDispatcherUsingLocks_v2();

        waitAndNotify.execute();
    }
}
