package com.s2s.concurrency;



/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 25-Aug-2005
 */

public class ThreadClashv2
{
   private  String   someData = "Hello World";
   private  boolean  continueToRun = true;

   public static void main(String[] args)
   {
      ThreadClashv2 threadClash = new ThreadClashv2();
      threadClash.execute();
   }

   private  void  execute()
   {
      Thread t1 = new Thread()
      {
         public   void  run()
         {
            while( ThreadClashv2.this.continueToRun )
            {
               try
               {
                  ThreadClashv2.this.someData = "Handler One";
                  this.sleep( 5 );
                  ThreadClashv2.this.someData = ThreadClashv2.this.someData + ", Handler One";
               }
               catch( InterruptedException e ){ }
            }
         }
      };

      Thread t2 = new Thread()
      {
         public   void  run()
         {
            while( ThreadClashv2.this.continueToRun )
            {
               try
               {
                  ThreadClashv2.this.someData = "Another Handler";
                  this.sleep( 5 );
                  ThreadClashv2.this.someData = ThreadClashv2.this.someData + ", Another Handler";
               }
               catch( InterruptedException e ){ }
            }
         }
      };

      Thread mainLoop = new Thread()
      {
         public   void  run()
         {
            for( int i = 0; i < 999; i++ )
            {
               System.out.println("[" + ThreadClashv2.this.someData + "]" );
            }
            ThreadClashv2.this.continueToRun = false;
            
            System.exit(0);
         }
      };

      mainLoop.start();
      t1.start();
      t2.start();
   }
}