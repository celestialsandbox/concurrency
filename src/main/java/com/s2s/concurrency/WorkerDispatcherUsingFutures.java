package com.s2s.concurrency;


/**
 * Title:        Java Courseware
 * Description:
 * Copyright:    Copyright (c) 2010
 * Company:      TROy Software Development
 *
 * @author Selvyn Wright
 * @version 1.0
 */
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WorkerDispatcherUsingFutures
{
    private final SharedDataStore dataStore = new SharedDataStore();
    private ReentrantLock itsLock = new ReentrantLock();
    
    private ExecutorService executor;
    private Callable<Boolean> callableProducer;
    private Future<Boolean> producerFuture;
    private Callable<Boolean> callableConsumer;
    private Future<Boolean> consumerFuture;

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {

        private final ArrayList<String> db = new ArrayList<>();
        
        public void clearDataStore()
        {
            itsLock.lock();
            db.clear();
            itsLock.unlock();
        }

        public void updateDB()
        {
            itsLock.lock();
            System.out.println("updating the data store");
            for (int i = 0; i < 100; i++)
            {
                db.add("Handler One" + i);
            }
            itsLock.unlock();
            System.out.println("update completed");
        }

        public  ArrayList<String> getData()
        {
            try
            {
                while( itsLock.tryLock(1000, TimeUnit.MILLISECONDS) )
                    ;
            } catch (InterruptedException ex)
            {
                Logger.getLogger(WorkerDispatcherUsingFutures.class.getName()).log(Level.SEVERE, null, ex);
            }
            return db;
        }
    }

    private class DispatcherThread
    {
        private Future<Boolean> producerFuture;
        
        public  DispatcherThread( Future<Boolean> future )
        {
            producerFuture = future;
        }
        
        public void execute()
        {
            try
            {
                for (int idx = 0; idx < 2; idx++)
                {
                    producerFuture.get();
                
                    printData();
                    
                    // see note above on design of getData()
                    dataStore.clearDataStore();
                    
                    producerFuture = executor.submit(callableProducer);
                }
            } catch (InterruptedException | ExecutionException ex)
            {
                Logger.getLogger(WorkerDispatcherUsingFutures.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void printData()
        {
            ArrayList<String> localDB = dataStore.getData();

            int length = localDB.size();

            localDB.forEach((str) ->
            {
                System.out.println( str );
            });
        }
    }
    
    public  void    execute()
    {
        executor = Executors.newFixedThreadPool(2);
        callableProducer = () -> {
            dataStore.updateDB();
            return false;
        };
        producerFuture = executor.submit(callableProducer);

        callableConsumer = () -> {
            DispatcherThread dt = new DispatcherThread( producerFuture );
            dt.execute();
            executor.shutdown();
            return false;
        };
        consumerFuture = executor.submit(callableConsumer);
    }

    public static void main(String[] args)
    {
        WorkerDispatcherUsingFutures waitAndNotify = new WorkerDispatcherUsingFutures();

        waitAndNotify.execute();
    }
}
