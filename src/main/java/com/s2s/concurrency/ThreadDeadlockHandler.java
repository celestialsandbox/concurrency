/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.lang.management.ThreadInfo;

/**
 *
 * @author Selvyn
 */
interface ThreadDeadlockHandler
{
    void handleDeadlock(final ThreadInfo[] deadlockedThreads);
}
