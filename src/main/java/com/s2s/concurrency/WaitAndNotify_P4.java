package com.s2s.concurrency;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 19-Sep-2005
 */

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WaitAndNotify_P4
{

    private final DispatcherThread dispatcher = new DispatcherThread();
    private final WorkerThread worker = new WorkerThread();
    private final SharedDataStore dataStore = new SharedDataStore();

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {
        private final ArrayList<String> db = new ArrayList<>();
        private boolean jobCompleted = false;

        public void clearDataStore()
        {
            synchronized (db)
            {
                db.clear();

                db.notify();
            }
        }

        public void updateDB()
        {
            System.out.println("updating the data store");

            synchronized (db)
            {
                for (int i = 0; i < 100; i++)
                {
                    db.add("Handler One" + i);
                }
                jobCompleted = true;
                db.notify();
                System.out.println("update completed");
                try
                {
                    db.wait();
                } catch (InterruptedException e){}
            }
        }

        // This is a better design, when this operation exits, the waiting thread now has access
        // to the datastore and the operation updateDB can now be called
        //
        // The call db.clear() above has to be done after the callee of getDB() has finished with
        // the data from this call
        //public Vector getData()
        public  ArrayList<String> getData()
        {
            while (!jobCompleted)
            {
                synchronized (db)
                {
                    System.out.println("getData() waiting for semaphore");
                    try
                    {
                        db.wait();
                    } catch (InterruptedException e){}
                }
            }
            jobCompleted = false;
            System.out.println("getData() Got the semaphore");

            return db;
        }
    }

    private class WorkerThread extends Thread
    {
        @Override
        public void run()
        {
            for (;;)
            {
                updateDataStore();
            }
        }

        private void updateDataStore()
        {
            dataStore.updateDB();
        }
    }

    private class DispatcherThread extends Thread
    {
        @Override
        public void run()
        {
            try
            {
                sleep( 500 ); // nomimal value, could be lower
                /* 
                    The sleep is needed to avoid a race condition for the 
                    initial synchronize(WaitAndNotify_P4.this.dataStore.db)
                */
            } catch (InterruptedException ex)
            {
                Logger.getLogger(WaitAndNotify_P4.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            for (int idx = 0; idx < 2; idx++)
            {
                printData();

                // see note above on design of getData()
                dataStore.clearDataStore();
            }
            System.exit(0);
        }

        private void printData()
        {
            ArrayList<String> localDB = dataStore.getData();

            int length = localDB.size();

            localDB.forEach((str) ->
            {
                System.out.println( str );
            });
        }
    }
    
    private void    initMonitor()
    {
        DeadlockDetector deadlockDetector = 
                new DeadlockDetector(new DeadlockHandlerToConsole(), 5, TimeUnit.SECONDS);
        deadlockDetector.start();
    }

    private void execute1()
    {
        initMonitor();
        
        System.out.println("execute 1");

        worker.start();
        dispatcher.start();
    }

    private void execute2()
    {
        initMonitor();
        
        System.out.println("execute 2");

        dispatcher.start();
        worker.start();
    }

    public static void main(String[] args)
    {
        int version = 1;
        WaitAndNotify_P4 waitAndNotify_P4 = new WaitAndNotify_P4();

        if (version == 1)
        {
            waitAndNotify_P4.execute1();
        } else
        {
            waitAndNotify_P4.execute2();
        }
    }
}
