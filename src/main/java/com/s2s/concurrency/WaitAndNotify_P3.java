package com.s2s.concurrency;


/**
 * @author Selvyn Wright
 * @version 1.0
 * Created on 19-Sep-2005
 */
import java.util.ArrayList;


public class WaitAndNotify_P3
{
    private final DispatcherThread dispatcher = new DispatcherThread();
    private final WorkerThread worker = new WorkerThread();
    private final SharedDataStore dataStore = new SharedDataStore();

    // Use this class to store data in.  The input/outputs are protected from
    // synchronisation errors...
    private class SharedDataStore
    {
        private final ArrayList<String> db = new ArrayList<>();
        private boolean jobCompleted = false;

        public void clearDataStore()
        {
            synchronized(this)
            {
                db.clear();

                notify();
            }
        }

        public void updateDB()
        {
            System.out.println("updating the data store");

            synchronized (db)
            {
                for (int i = 0; i < 100; i++)
                {
                    db.add("Handler One" + i);
                }
                jobCompleted = true;
                db.notify();
                System.out.println("update completed");
                try
                {
                    db.wait();
                } catch (InterruptedException e){}
            }
        }

        // This is a better design, when this operation exits, the waiting thread now has access
        // to the datastore and the operation updateDB can now be called
        //
        // The call db.clear() above has to be done after the callee of getDB() has finished with
        // the data from this call
        //public Vector getData()
        public  ArrayList<String> getData()
        {
            while (!jobCompleted)
            {
                System.out.println("getData() waiting for semaphore");
                synchronized (db)
                {
                    try
                    {
                        db.wait();
                    } catch (InterruptedException e){}
                }
            }
            jobCompleted = false;
            System.out.println("getData() Got the semaphore");

            return db;
        }
    }

    private class WorkerThread extends Thread
    {
        @Override
        public void run()
        {
            for (;;)
            {
                updateDataStore();
            }
        }

        private void updateDataStore()
        {
            dataStore.updateDB();
        }
    }

    private class DispatcherThread extends Thread
    {

        @Override
        public void run()
        {
            for (int idx = 0; idx < 2; idx++)
            {
                printData();

                // see note above on design of getData()
                dataStore.clearDataStore();
            }
            System.exit(0);
        }

        private void printData()
        {
            ArrayList<String> localDB = dataStore.getData();

            int length = localDB.size();

            for( String str: localDB )
            {
                System.out.println( str );
            }
        }
    }

    private void execute1()
    {
        System.out.println("execute 1");

        worker.start();
        dispatcher.start();
    }

    private void execute2()
    {
        System.out.println("execute 2");

        dispatcher.start();
        worker.start();
    }

    public static void main(String[] args)
    {
        int version = 2;
        WaitAndNotify_P3 waitAndNotify = new WaitAndNotify_P3();

        if (version == 1)
        {
            waitAndNotify.execute1();
        } else
        {
            waitAndNotify.execute2();
        }
    }
}
