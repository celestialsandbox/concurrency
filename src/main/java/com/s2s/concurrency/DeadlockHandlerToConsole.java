/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.s2s.concurrency;

import java.lang.management.ThreadInfo;
import java.util.Map;

/**
 *
 * @author Selvyn
 */
public class DeadlockHandlerToConsole implements ThreadDeadlockHandler {

    @Override
    public void handleDeadlock(final ThreadInfo[] deadlockedThreads) 
    {
        if (deadlockedThreads != null) 
        {
            System.err.println("Deadlock detected!");
        
            Map<Thread, StackTraceElement[]> stackTraceMap = Thread.getAllStackTraces();
            for(ThreadInfo threadInfo : deadlockedThreads) 
            {
                if (threadInfo != null) 
                {
                    displayThreadInfo(threadInfo);
                }
            }
        }
    }
    
    private void    displayThreadInfo( ThreadInfo threadInfo )
    {
        for(Thread thread : Thread.getAllStackTraces().keySet()) 
        {
            if (thread.getId() == threadInfo.getThreadId()) 
            {
                System.err.println(threadInfo.toString().trim());
                for (StackTraceElement ste : thread.getStackTrace()) 
                {
                    System.err.println("\t" + ste.toString().trim());
                }
            }
        }
    }
}
